# SmolCluster

SmolCluster allows you to power a set of Raspberry Pi Zero 2 w computers, without worrying about power outages that can and will corrupt your SD cards.

Each Raspberry Pi Zero 2 w computer is powered from a single 18650 lithium ion battery and SmolCluster boards may support up to 4 computers, each powered from a single battery.

The SmolCluster board itself is powered from USB-C and the power is then routed to 4 charge controllers, each managing 1 18650 lithium ion battery.

Each battery will then power a 5V boost converter that will finally power the Raspberry Pi Zero 2 w computer.

4 ATTiny85 microcontrollers will acquire the voltage of each battery and tell each Raspberry Pi the voltage via USB interface.

A program will run on each Raspberry Pi to speak periodically with the ATTiny85 and ask for the battery voltage and shutdown the Raspberry Pi if the voltage is below a threshold.