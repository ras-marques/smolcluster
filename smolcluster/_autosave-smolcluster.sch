EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Battery_Management:MAX1811 U1
U 1 1 61B299D0
P 3800 3900
F 0 "U1" H 3850 4367 50  0000 C CNN
F 1 "MAX1811" H 3850 4276 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4050 3550 50  0001 L CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX1811.pdf" H 3800 3200 50  0001 C CNN
	1    3800 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 61B2A3F2
P 3800 4450
F 0 "#PWR0101" H 3800 4200 50  0001 C CNN
F 1 "GND" H 3805 4277 50  0000 C CNN
F 2 "" H 3800 4450 50  0001 C CNN
F 3 "" H 3800 4450 50  0001 C CNN
	1    3800 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 4300 3800 4400
Wire Wire Line
	3800 4400 3900 4400
Wire Wire Line
	3900 4400 3900 4300
Connection ~ 3800 4400
Wire Wire Line
	3800 4400 3800 4450
Wire Wire Line
	3400 4000 3150 4000
Wire Wire Line
	3150 4000 3150 4100
Wire Wire Line
	3150 4400 3800 4400
Wire Wire Line
	3400 4100 3150 4100
Connection ~ 3150 4100
Wire Wire Line
	3150 4100 3150 4400
$Comp
L Device:C C1
U 1 1 61B2CA75
P 2900 3850
F 0 "C1" H 3015 3896 50  0000 L CNN
F 1 "4.7u" H 3015 3805 50  0000 L CNN
F 2 "" H 2938 3700 50  0001 C CNN
F 3 "~" H 2900 3850 50  0001 C CNN
	1    2900 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 3800 3400 3700
Wire Wire Line
	3400 3700 2900 3700
Connection ~ 3400 3700
Wire Wire Line
	2900 4000 2900 4400
Wire Wire Line
	2900 4400 3150 4400
Connection ~ 3150 4400
$Comp
L Connector:USB_C_Plug_USB2.0 P1
U 1 1 61B2E226
P 1850 4100
F 0 "P1" H 1957 4967 50  0000 C CNN
F 1 "USB_C_Plug_USB2.0" H 1957 4876 50  0000 C CNN
F 2 "" H 2000 4100 50  0001 C CNN
F 3 "https://www.usb.org/sites/default/files/documents/usb_type-c.zip" H 2000 4100 50  0001 C CNN
	1    1850 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 61B31B2F
P 1850 5000
F 0 "#PWR0102" H 1850 4750 50  0001 C CNN
F 1 "GND" H 1855 4827 50  0000 C CNN
F 2 "" H 1850 5000 50  0001 C CNN
F 3 "" H 1850 5000 50  0001 C CNN
	1    1850 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 3500 2900 3500
Wire Wire Line
	2900 3500 2900 3700
Connection ~ 2900 3700
$Comp
L MCU_Microchip_ATtiny:ATtiny85-20SU U2
U 1 1 61D6E3E4
P 3750 5600
F 0 "U2" H 3221 5646 50  0000 R CNN
F 1 "ATtiny85-20SU" H 3221 5555 50  0000 R CNN
F 2 "Package_SO:SOIJ-8_5.3x5.3mm_P1.27mm" H 3750 5600 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/atmel-2586-avr-8-bit-microcontroller-attiny25-attiny45-attiny85_datasheet.pdf" H 3750 5600 50  0001 C CNN
	1    3750 5600
	1    0    0    -1  
$EndComp
$EndSCHEMATC
